param ($device, $name, $status, $down, $message)

#Authors: Jordan Burch:jordan@yobo.dev & Patrick Barron:patrick@yobo.dev
#license: MIT

$from_number = '18045555555'
$token = 'somelongtoken'
$sid = 'somelongsid'

[System.Collections.ArrayList]$recipient_numbers = @()

# user 1
$recipient_numbers.Add("1804555551");

# user 2
$recipient_numbers.Add("1804555552");


foreach ($to_number in $recipient_numbers){
    $url = "https://api.twilio.com/2010-04-01/Accounts/$sid/Messages.json"
    $params = @{
        To   = $to_number;
        From = $from_number;
        Body = "Device: $device, Name: $name, Status: $status, Down: $down, Message: $message";
    }

    # Create a credential object for HTTP basic auth
    $p = $token | ConvertTo-SecureString -asPlainText -Force
    $credential = New-Object System.Management.Automation.PSCredential($sid, $p)

    # Make API request, selecting JSON properties from response
    Invoke-WebRequest $url -Method Post -Credential $credential -Body $params -UseBasicParsing |
    ConvertFrom-Json | Select-Object sid, body;
}
